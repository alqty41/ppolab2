using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawOnAwake : MonoBehaviour
{
  
    void Awake()
    {
        StartCoroutine(Ien());
    }

    IEnumerator Ien()
    {
        yield return new WaitForSeconds(1f);
        GameObject.Find("Controller").GetComponent<MainController>().DrawData();
    }
}
