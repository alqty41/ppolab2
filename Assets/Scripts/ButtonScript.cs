using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;

public class ButtonScript : MonoBehaviour
{

    private DatabaseReference db;
    private void Start()
    {
        db = FirebaseDatabase.DefaultInstance.RootReference;
    }

    public void Update()
    {
        if(id == "-1")
        {
            
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 0);
        }
        else
        {
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 255);

        }
    }
    public string id;

    public void OnClick()
    {
        db.Child("Data").Child(id).SetValueAsync(null);
    }
}
