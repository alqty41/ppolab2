using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataItem
{
    public string Name { get; set; }
    public string Position { get; set; }
    public string Salary { get; set; }

    public DataItem(string name, string position, string salary)
    {
        this.Name = name;
        this.Position = position;
        this.Salary = salary;
    }
    public DataItem()
    {

    }
}
