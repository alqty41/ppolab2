using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using System.Collections;
using System.Collections.Generic;

public class MainController : MonoBehaviour
{
    public DataItem[] test = new DataItem[10];
    public InputField Name;
    public InputField position;
    public InputField salary;
    public int totalusers = 0;
    private DatabaseReference db;
    private List<DataItem> data = new List<DataItem>();
    private string[] keys = new string[10];
    public GameObject setCanvas;
    public GameObject getCanvas;
    public GameObject cloneObj;
    public GameObject dbList;
    private GameObject newData;
    public Text[] dbTexts;
    public ButtonScript[] dbButtons;
    // Start is called before the first frame update
    void Start()
    {
        db = FirebaseDatabase.DefaultInstance.RootReference;
        GetTotalUsersFromDB();

    }
    private void GetTotalUsersFromDB()
    {
        db.Child("Counter").Child("TotalUsers").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(snapshot);
                if (snapshot.Value is null)
                {
                    totalusers = 0;
                    db.Child("Counter").Child("TotalUsers").SetValueAsync(totalusers);
                }
                else
                {
                    totalusers = int.Parse(snapshot.Value.ToString());
                }
                // Do something with snapshot...
            }
        });
    }
    public void ReadDB()
    {
        getCanvas.gameObject.SetActive(true);
        setCanvas.gameObject.SetActive(false);

        int i = 0;
        db.Child("Data").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(snapshot);
                if (snapshot.Value is null)
                {
                   
                }
                else
                {
                 
                    foreach (DataSnapshot item in snapshot.Children)
                    {
                        
                        string n = " ", p = " ", s = "";
                        foreach (DataSnapshot item1 in item.Children)
                        {
                            if (item1.Value is null)
                            {
                                i--;
                                break;
                            }
                            else
                            {


                                if (item1.Key == "Name")
                                {
                                    n = item1.Value.ToString();
                                }
                                else if (item1.Key == "Position")
                                {
                                    p = item1.Value.ToString();
                                }
                                else
                                {
                                    s = item1.Value.ToString();
                                }
                                
                            }
                            
                        }
                        test[i] = new DataItem(n, p, s);
                        keys[i] = item.Key;
                        i++;

                    }
                }
            }
        });
    }
    public void DrawData()
    {
        for (int i = 0; i < 2; i++)
        {
           // Debug.Log(test[i].Name + " " + test[i].Position + " " + test[i].Salary);
           
            dbTexts[i * 3].text = test[i].Name;
            dbTexts[i * 3 + 1].text = test[i].Position;
            dbTexts[i * 3 + 2].text = test[i].Salary;
            dbButtons[i].id = keys[i];
        }
    }
    public void AddToDb()
    {
        
        db.Child("Data").Child(totalusers.ToString()).Child("Name").SetValueAsync(Name.text);
        db.Child("Data").Child(totalusers.ToString()).Child("Position").SetValueAsync(position.text);
        db.Child("Data").Child(totalusers.ToString()).Child("Salary").SetValueAsync(salary.text);
        totalusers++;
        db.Child("Counter").Child("TotalUsers").SetValueAsync(totalusers);
    }
    public void BackToMenu()
    {
        getCanvas.gameObject.SetActive(false);
        setCanvas.gameObject.SetActive(true);
    }
}
